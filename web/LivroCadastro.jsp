<%-- 
    Document   : LivroCadastro
    Created on : 26/03/2017, 18:06:32
    Author     : Endrigo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Livros</title>
    </head>
    <body>
        <h1>Cadastro de Livros</h1>
        <form action="Controller">
            <input type="hidden" name="acao" value="CadastrarLivro"/>
            <input type="hidden" name="id" value="<c:out value="${livro.id}" />"/>
            <h4>Selecione um autor. Caso não esteja listado, faça o seu cadastro.</h4>
            <c:forEach var="autor" items="${autores}">
                <div><input type="checkbox" name="autores" <c:if test="${autor.checado}">checked</c:if> value="${autor.id}"/><label>${autor.nome}</label></div>
            </c:forEach>
            <br><label>_Título:_</label><input type="text" name="titulo" value="<c:out value="${livro.titulo}" />">
            <br><label>Edição:_ </label><input type="text" name="edicao" value="<c:out value="${livro.edicao}" />">
        <button type="submit">Cadastrar</button>
        </form>
    </body>
</html>
