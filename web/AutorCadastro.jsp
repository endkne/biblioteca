<%-- 
    Document   : AutorCadastro
    Created on : 26/03/2017, 17:13:23
    Author     : Endrigo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cadastro de Autores</title>
    </head>
    <body>
        <h1>Cadastro de Autor</h1>
        <form action="Controller" method="post">
            <input type="hidden" name="acao" value="CadastrarAutor"/>
            <input type="hidden" name="id" <c:out value="${autor.id}"/>
            <label>Nome: </label><input type="text" name="nome" <c:out value="${autor.nome}"/>
            <button type="submit">Cadastrar</button>
        </form>
    </body>
</html>
