<%-- 
    Document   : AutorLista
    Created on : 26/03/2017, 16:06:25
    Author     : Endrigo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Autores</title>
        <style>
            table tr{
                text-align: left!important;
                
            }
            table, table tr td, table tr th{
                border: solid 1px;
            }
        </style>
    </head>
    <body>
        <h1>Autores</h1>
        <table>
            <tr>
                <th>Id</th>                
                <th>Nome</th>
                <th>Título(s)</th>
            </tr>
        <c:forEach var="autor" items="${autores}">
            <tr>
                <td>
                    <a href="Controller?acao=SalvarAutor&id=${autor.id}">${autor.id}</a>                    
                </td>
                <td>
                    ${autor.nome}
                </td>
                <td>
                    ${autor.livros}
                </td>
                <td>
                    <a href="Controller?acao=ExcluirAutor&id=${autor.id}">Excluir</a>                    
                </td>
            </tr>
            
        </c:forEach>
        </table>
        <br></br>
        <a href="index.html">Voltar</a>
        </body>
</html>
