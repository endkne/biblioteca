package DAO;

import bean.Autor;
import bean.Livro;
import fabricaDeSessoes.HibernateUtil;
import java.util.ArrayList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

/**
 *
 * @author endrigo
 */
public class LivroDAO {

    static SessionFactory fabrica = HibernateUtil.getSessionFactory();

    public void adicionarLivro(List<Integer> autoresId, String titulo, int edicao) {
        
        List<Autor> autores = new ArrayList();
        AutorDAO autorDAO = new AutorDAO();
        
        for(int autorId : autoresId){
            autores.add(autorDAO.retornaAutor(autorId));
        } 
            
        Livro livro = new Livro();
        livro.setAutor(autores);
        livro.setTitulo(titulo);
        livro.setEdicao(edicao);

        
        Session session = fabrica.openSession();
        session.beginTransaction();

        session.save(livro);

        session.getTransaction().commit();
        session.close();
    }
    public void atualizarLivro(int id, List<Integer> autoresId, String titulo, int edicao) {
        Livro livro = this.retornaLivro(id);

        List<Autor> autores = new ArrayList();
        AutorDAO autorDAO = new AutorDAO();
        
        for(int autorId : autoresId){
            autores.add(autorDAO.retornaAutor(autorId));
        } 
        
        livro.setAutor(autores);
        livro.setTitulo(titulo);
        livro.setEdicao(edicao);
        
        Session session = fabrica.openSession();
        session.beginTransaction();

        session.update(livro);

        session.getTransaction().commit();
        session.close();
    }
    
    public void removerLivro(int id) {
        Livro livro = this.retornaLivro(id);
        
        Session session = fabrica.openSession();
        session.beginTransaction();

        session.delete(livro);

        session.getTransaction().commit();
        session.close();
    }
    
    
    public Livro retornaLivro(int id) {
        Session session = fabrica.openSession();

        Query q = session.createQuery("from Livro where id = :livro");
        Livro livro = (Livro) q.setParameter("livro", id).uniqueResult();
        
        session.close();
        return livro;
    }
    
    public List<Livro> retornaLivros() {
        List<Livro> retorno;
        Session session = fabrica.openSession();

        Query q = session.createQuery("from Livro");
        retorno = q.list();
        session.close();
        return retorno;
    }
}
