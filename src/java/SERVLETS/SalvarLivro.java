package SERVLETS;

import DAO.AutorDAO;
import DAO.LivroDAO;
import bean.Autor;
import bean.Livro;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Elaine / Endrigo
 */
public class SalvarLivro implements Acao {

    public String executar(HttpServletRequest request, HttpServletResponse response) {
        AutorDAO autorDAO = new AutorDAO();
        LivroDAO livroDAO = new LivroDAO();

        String id = request.getParameter("id");

        Livro livro;
        List<Autor> autores = autorDAO.retornaAutores();

        if (id == null || id == "") {
            livro = new Livro();
        } else {
            livro = livroDAO.retornaLivro(Integer.parseInt(id));
            for (Autor autor : autores) {
                for (Autor autorAtual : livro.getAutor()) {
                    if (autor.getId() == autorAtual.getId()) {
                        autor.setChecado(true);
                    }
                }
            }
        }

        request.setAttribute("livro", livro);

        request.setAttribute("autores", autores);

        return "/LivroCadastro.jsp";
    }

}
