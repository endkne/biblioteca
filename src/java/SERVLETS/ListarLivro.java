package SERVLETS;

import DAO.LivroDAO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Elaine / Endrigo
 */
public class ListarLivro implements Acao {
    public String executar(HttpServletRequest request, HttpServletResponse response){
        LivroDAO livroDAO = new LivroDAO();
        
        request.setAttribute("livros", livroDAO.retornaLivros());

        return "/LivroLista.jsp";
    }
}
